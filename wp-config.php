<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'diyfactory' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Z97=y},.6|ZeL7]Q(*U7c>GCq:W{ki>Gib&hB=YDW%!OIB)}HCf$CGEs_OV`W){b' );
define( 'SECURE_AUTH_KEY',  'VtE%O#+Gd6#XN)+d}x,k^y/hwi}x>p<[-hk6q!j?Mj{nAT:Ya9u5<~<9P]Y[_!2n' );
define( 'LOGGED_IN_KEY',    '6RG??/7A{2SIppB#No},ykF8h|YM?zC7j`8L$>80@lvqOk[q+*Z| =Z:#T|B08O(' );
define( 'NONCE_KEY',        'sQR-exDA}HePcg:)zc=c1n Jm>:*/YiJ.7ZmqbmI#=bsC#{b[H4s^Q2bN80]>|D,' );
define( 'AUTH_SALT',        'k]W$R JIAeM9V&l~{yf,Ks+S~2CKF?Q$rd[.c1u(Bm.CLI^oE_]5fv~p(yE)@G3H' );
define( 'SECURE_AUTH_SALT', 'D4Rm4-BcP.$dF)PyCibn_cMYa$7/<l~E[IMxX9%<da1IS;SsQWr]SZ d>qS,YU24' );
define( 'LOGGED_IN_SALT',   ']R6ndY(.z$[5+V=iJ_u%>03H44k`zoR1S$F-02s}mv/y/*wv;]y`nIr1CXODxiK#' );
define( 'NONCE_SALT',       '9!XF7]YOTyE(V~H|1LhnC6.sT`6}tJ6(vniT{bb>x l&79&Fct4VY0zR1IyleGlO' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
