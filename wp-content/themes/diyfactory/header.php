<!DOCTYPE html>
<html lang="ja">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <meta name="viewport" content="width=device-width,user-scalable=no,maximum-scale=1" />
    <meta name="description" content="DIY FACTORYのオンラインショップ本店です。DIYに必要な工具やガーデニング用品、塗料が日本最大級の品揃え。5,000円以上のお買い物で送料無料です（離島除く）。">
    <meta name="keywords" content="DIY,DIY FACTORY ONLINE SHOP,DIYツールドットコム,DIYツール,工具,diytool,diy-tool.com,電動工具,ガーデニング,園芸,">
    <meta name="thumbnail" content="" />
    <meta property="og:title" content="DIY工具・ガーデニンググッズ通販｜DIY FACTORY" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="common/img/fb_icon.png" />
    <meta property="og:url" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="DIY FACTORY" />
    <title>DIY工具・ガーデニンググッズ通販｜DIY FACTORY</title>

    <?php wp_head()?>
    
    <link rel="canonical" href="https://shop.diyfactory.jp/" />
    <link rel="icon" type="image/x-icon" href="https://shop.diyfactory.jp/landing/top/common/img/favicon.ico">
</head>

<body id="top">

    <div class="container">


        <!-- #################### sp_header #################### -->

        <div class="sp_header_sp flex flex--bet">
            <div class="sp_header_nav sb-slide clearfix">
                <h1>
                    <a href="https://shop.diyfactory.jp/"><img src="<?php echo get_template_directory_uri(); ?>/common/img/df_logo.svg" alt="DIY FACTORY" /></a>
                </h1>
                <div class="navi-l">
                    <ul>
                        <li class="navi_right01">
                            <a href="#item"><img src="<?php echo get_template_directory_uri(); ?>/common/img/cart_icon001.svg" alt="カート"></a>
                        </li>
                        <li class="navi_right02">
                            <a href="#about"><img src="<?php echo get_template_directory_uri(); ?>/common/img/favorite_icon001.svg" alt="お気に入り"></a>
                        </li>
                </div>
                <ul class="navi-r">
                    <li class="sb-toggle-right">
                        <a href="#"></a>
                    </li>
                </ul>
            </div>

        </div>

        <!-- #################### sp_header #################### -->


        <!-- ################ pc_header ################ -->

        <div class="header_sp">
            <div class="header_sp02 clearfix">
                <div class="logo_sp">
                    <h1>
                        <a href="＃" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/df_logo.svg" alt="DIY FACTORY"></a>
                    </h1>
                </div>

                <div class="header_sp03 clearfix">

                    <div class="global_nav">
                        <ul>
                            <li><a href="#item">店舗</a></li>
                            <li><a href="#insta">会員登録</a></li>
                            <li><a href="#insta">ログイン</a></li>
                        </ul>
                    </div>

                    <div class="header_cat clearfix">

                        <ul>
                            <li><a href="#" class="navi">カテゴリ</a>
                                <div class="megaWrap">
                                    <div class="megaContentWrap">

                                        <div class="search_list">
                                            <h3>パーツから探す</h3>
                                            <ul>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/parts/search_pa_img001.png" alt="木材">木材</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/parts/search_pa_img001.png" alt="塗料">塗料</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/parts/search_pa_img001.png" alt="金具">金具</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/parts/search_pa_img001.png" alt="工具">工具</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/parts/search_pa_img001.png" alt="装飾">装飾</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="search_list">
                                            <h3>コーデから探す</h3>
                                            <ul>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/coordinate/search_co_img001.png" alt="収納">収納</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/coordinate/search_co_img001.png" alt="家具">家具</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/coordinate/search_co_img001.png" alt="インテリア">インテリア</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/coordinate/search_co_img001.png" alt="エクステリア">エクステリア</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="search_list">
                                            <h3>シーンから探す</h3>
                                            <ul>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="玄関">玄関</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="トイレ">トイレ</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="キッチン">キッチン</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="リビング">リビング・ダイニング</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="ガレージ">ガレージ</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="search_list">
                                            <h3 class="pc"></h3>
                                            <ul>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="子ども部屋">子ども部屋</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="寝室">寝室</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="ベランダ">ベランダ</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="庭">庭</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="search_list">
                                            <h3>プロジェクトから探す</h3>
                                            <ul>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/project/search_pr_img001.png" alt="KIDS">DIY×KIDS</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/project/search_pr_img001.png" alt="賃貸">DIY×賃貸</a>
                                                </li>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/project/search_pr_img001.png" alt="GREEN">DIY×GREEN</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="search_list">
                                            <h3>DIYerから探す</h3>
                                            <ul>
                                                <li>
                                                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/diyer/search_di_img001.png" alt="コーラル">コーラル</a>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="sub_nav">
                        <ul>
                            <li>
                                <a href="#about"><img src="<?php echo get_template_directory_uri(); ?>/common/img/favorite_icon001.svg" alt="お気に入り"><br>お気に入り</a>
                            </li>
                            <li>
                                <a href="#item"><img src="<?php echo get_template_directory_uri(); ?>/common/img/cart_icon001.svg" alt="カート"><br>カート</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <!-- ################ pc_header ################ -->


        <p id="df_top"><a href="#top">top</a></p>


        <!-- ######### drawer_menu ######### -->

        <div id="sb-site2">