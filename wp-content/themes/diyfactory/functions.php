<?php 

/**
 * Adding the CSS and JS files
 */

function gt_setup()
{
    wp_enqueue_style('index', get_theme_file_uri('/common/css/index.css'), NULL, microtime(), 'all');
    wp_enqueue_style('commons', get_theme_file_uri('/common/css/common.css'), NULL, microtime(), 'all');
    wp_enqueue_style('common_sp', get_theme_file_uri('/common/css/common_sp.css'), NULL, microtime(), 'all');
    wp_enqueue_style('slick.css', get_theme_file_uri('/common/css/slick.css'), NULL, microtime(), 'all');
    wp_enqueue_style('slick-theme', get_theme_file_uri('/common/css/slick-theme.css'), NULL, microtime(), 'all');
    wp_enqueue_style('slidebars', get_theme_file_uri('/common/css/slidebars.min.css'), NULL, microtime(), 'all');
    wp_enqueue_style('jquery.bxslider', get_theme_file_uri('/common/css/jquery.bxslider.css'), NULL, microtime(), 'all');
    wp_enqueue_style('google-font', 'https://fonts.googleapis.com/css?family=Roboto+Slab');

    wp_enqueue_script('jquery.min.js', 'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', NULL, microtime(), true);
    wp_enqueue_script('jquery-ui.min.js', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js', NULL, microtime(), true);
    wp_enqueue_script('slick.min.js', get_theme_file_uri('/common/js/slick.min.js'), NULL, microtime(), true);
    wp_enqueue_script('jquery.bxslider.min.js', get_theme_file_uri('/common/js/jquery.bxslider.min.js'), NULL, microtime(), true);
    wp_enqueue_script('slidebars.min.js', get_theme_file_uri('/common/js/slidebars.min.js'), NULL, microtime(), true);


}

add_action('wp_enqueue_scripts', 'gt_setup');
