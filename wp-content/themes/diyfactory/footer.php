<div class="footer">

                <p class="footer_pagetop_sp sp"><a href="#top">このページのトップへ</a></p>

                <div class="footer_sp02 clearfix">

                    <div class="footer_sp02_right clearfix">
                        <div class="footer_list clearfix">
                            <h5>インフォメーション</h5>
                            <ul>
                                <li><a href="https://shop.diyfactory.jp/information?id=4">運営会社について</a></li>
                                <li><a href="https://shop.diyfactory.jp/information?id=5">利用規約</a></li>
                                <li><a href="https://shop.diyfactory.jp/information?id=6">ご利用ガイド</a></li>
                                <li><a href="https://shop.diyfactory.jp/information?id=3">プライバシーポリシー</a></li>
                            </ul>
                        </div>
                        <div class="footer_list clearfix">
                            <h5>カスタマーサービス</h5>
                            <ul>
                                <li><a href="https://shop.diyfactory.jp/index.php?route=information/faq">よくあるご質問</a></li>
                                <li><a href="https://b-dash2.diyfactory.jp/form/contact?type=dfb">メール問い合わせ</a></li>
                                <li><a href="https://shop.diyfactory.jp/information?id=7">送料について</a></li>
                                <li><a href="https://shop.diyfactory.jp/information?id=9">発送日について</a></li>
                                <li><a href="https://shop.diyfactory.jp/information?id=8">お支払い方法</a></li>
                                <li><a href="https://shop.diyfactory.jp/information?id=13">領収書発行</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="footer_sp02_left">
                        <h4>
                            <a href="https://shop.diyfactory.jp/"><img src="<?php echo get_template_directory_uri(); ?>/common/img/df_online_logo.svg" alt="DIY FACTORY オンラインショップ" /></a>
                        </h4>
                        <h5>運営会社／株式会社 大都</h5>
                        <p>〒544-0025<br> 大阪府大阪市生野区生野東2-5-3
                        </p>
                    </div>

                </div>
                <div class="copy">
                    <p class="center">Copyright DIY FACTORY All right reserved.</p>
                </div>
            </div>

            <!-- #################### footer #################### -->


        </div>

        <!-- ######### drawer_menu ######### -->



    </div>


    <!-- ######### sp_sidenav ######### -->



    <!-- ######### sp_sidenav ######### -->


<?php wp_footer(); ?>
<!--[if lt IE 9]>
<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>
(function($) {
    $(document).ready(function() {
        $.slidebars();
    });
})(jQuery);

(jQuery);
$(function() {
    $('a[href^=#]').click(function() {
        var speed = 500;
        var href = $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({
            scrollTop: position
        }, speed, "swing");
        return false;
    });
});

var startPos = 0,
    winScrollTop = 0;
$(window).on('scroll', function() {
    winScrollTop = $(this).scrollTop();
    if (winScrollTop >= startPos) {
        if (winScrollTop >= 200) {
            $('.sp_header_sp').addClass('hide');
        }
    } else {
        $('.sp_header_sp').removeClass('hide');
    }
    startPos = winScrollTop;
});

$(function() {
    var topBtn = $('#df_top');
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});

$(function() {

    $('.header_cat > ul > li').hover(function() {
            megaMenu = $(this).children('.megaWrap');
            gnavi = $(this).children('.navi');
            megaMenu.each(
                function() {
                    megaMenu.css({
                        display: 'block',
                        opacity: '0'
                    }).stop().animate({
                        opacity: '1'
                    }, 600);
                });
        },
        function() {
            megaMenu.css({
                display: 'none'
            });
        });
});

$(document).ready(function() {
    $('.ac_list_sp').show();
    $('h3.ac_title').addClass('ac').click(function() {
        $(this).toggleClass('ac').next('.ac_list_sp').slideToggle();
        return false;
    });
});

$(function() {
    $('.pc_slide_inner').slick({
        dots: true,
        centerMode: true,
        centerPadding: '120px',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
    });
});

$(function() {
    $('.sp_slide_inner').slick({
        dots: true,
        centerPadding: '120px',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
    });
});

$(function() {
    $('#slider1').bxSlider({
        auto: true,
        pause: 4000,
        speed: 1000,
        mode: 'fade',
        captions: false
    });
});
$(function() {
    $('#slider2').bxSlider({
        auto: true,
        speed: 2000,
        mode: 'fade',
        captions: false
    });
});
</script>
</body>

</html>