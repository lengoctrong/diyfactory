<?php get_header(); ?>

<!-- ################ index_contents ################ -->

<div class="index_contents_sp">


<!-- ################ main_visual ################ -->

<div class="index_main_sp">
    <div class="pc_slide_inner">
        <div>
            <p>
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/slide_img/pc_slide_img001.png" alt="スライドショー画像その1" /></a>
            </p>
        </div>
        <div>
            <p>
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/slide_img/pc_slide_img002.png" alt="スライドショー画像その2" /></a>
            </p>
        </div>
        <div>
            <p>
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/slide_img/pc_slide_img003.png" alt="スライドショー画像その3" /></a>
            </p>
        </div>
        <div>
            <p>
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/slide_img/pc_slide_img004.png" alt="スライドショー画像その4" /></a>
            </p>
        </div>
    </div>
</div>

<div class="sp_index_main_sp">
    <div class="sp_slide_inner">
        <div>
            <p>
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/slide_img/sp/sp_slide_img001.png" alt="スマホ用スライドショー画像その1" /></a>
            </p>
        </div>
        <div>
            <p>
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/slide_img/sp/sp_slide_img002.png" alt="スマホ用スライドショー画像その2" /></a>
            </p>
        </div>
        <div>
            <p>
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/slide_img/sp/sp_slide_img003.png" alt="スマホ用スライドショー画像その3" /></a>
            </p>
        </div>
        <div>
            <p>
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/slide_img/sp/sp_slide_img004.png" alt="スマホ用スライドショー画像その4" /></a>
            </p>
        </div>
    </div>
</div>

<!-- ################ main_visual ################ -->


<!-- ################ news ################ -->

<div class="index_news_sp clearfix">
    <h2>NEWS</h2>
    <ul>
        <li><a href="#">リンクテキストリンクテキストリンクテキストリンクテキストリンクテキスト</a></li>
        <li><a href="#">リンクテキストリンクテキストリンクテキストリンクテキストリンクテキスト</a></li>
    </ul>
</div>

<!-- ################ news ################ -->


<!-- ################ ranking ################ -->

<div class="index_ranking_sp">
    <div class="title_sp01 ranking_title">
        <div class="title_sp01_02">
            <h2>RANKING</h2>
            <p class="center">DIY FACTORY スタッフおすすめランキング　6/18 update</p>
        </div>
    </div>

    <div class="index_ranking_sp02">

        <div class="ranking_list">
            <p class="rank_no1">1</p>
            <p class="rank_img">
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/ranking_img/ranking_img001.png" alt=""></a>
            </p>
            <h3><a href="#">商品名テキスト商品名テキスト商品名テキスト商品名テキスト</a></h3>
            <p class="price_style"><span class="price_style001">10,000円</span><span class="price_style002">(税込)</span></p>
        </div>

        <div class="ranking_list">
            <p class="rank_no2">2</p>
            <p class="rank_img">
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/ranking_img/ranking_img001.png" alt=""></a>
            </p>
            <h3><a href="#">商品名テキスト商品名テキスト商品名テキスト商品名テキスト</a></h3>
            <p class="price_style"><span class="price_style001">10,000円</span><span class="price_style002">(税込)</span></p>
        </div>

        <div class="ranking_list">
            <p class="rank_no3">3</p>
            <p class="rank_img">
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/ranking_img/ranking_img001.png" alt=""></a>
            </p>
            <h3><a href="#">商品名テキスト商品名テキスト商品名テキスト商品名テキスト</a></h3>
            <p class="price_style"><span class="price_style001">10,000円</span><span class="price_style002">(税込)</span></p>
        </div>

        <div class="ranking_list">
            <p class="rank_img">
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/ranking_img/ranking_img001.png" alt=""></a>
            </p>
            <h3><a href="#">商品名テキスト商品名テキスト商品名テキスト商品名テキスト</a></h3>
            <p class="price_style"><span class="price_style001">10,000円</span><span class="price_style002">(税込)</span></p>
        </div>

        <div class="ranking_list">
            <p class="rank_img">
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/ranking_img/ranking_img001.png" alt=""></a>
            </p>
            <h3><a href="#">商品名テキスト商品名テキスト商品名テキスト商品名テキスト</a></h3>
            <p class="price_style"><span class="price_style001">10,000円</span><span class="price_style002">(税込)</span></p>
        </div>

        <div class="ranking_list">
            <p class="rank_img">
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/ranking_img/ranking_img001.png" alt=""></a>
            </p>
            <h3><a href="#">商品名テキスト商品名テキスト商品名テキスト商品名テキスト</a></h3>
            <p class="price_style"><span class="price_style001">10,000円</span><span class="price_style002">(税込)</span></p>
        </div>

        <div class="ranking_list">
            <p class="rank_img">
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/ranking_img/ranking_img001.png" alt=""></a>
            </p>
            <h3><a href="#">商品名テキスト商品名テキスト商品名テキスト商品名テキスト</a></h3>
            <p class="price_style"><span class="price_style001">10,000円</span><span class="price_style002">(税込)</span></p>
        </div>

        <div class="ranking_list">
            <p class="rank_img">
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/ranking_img/ranking_img001.png" alt=""></a>
            </p>
            <h3><a href="#">商品名テキスト商品名テキスト商品名テキスト商品名テキスト</a></h3>
            <p class="price_style"><span class="price_style001">10,000円</span><span class="price_style002">(税込)</span></p>
        </div>

        <div class="ranking_list">
            <p class="rank_img">
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/ranking_img/ranking_img001.png" alt=""></a>
            </p>
            <h3><a href="#">商品名テキスト商品名テキスト商品名テキスト商品名テキスト</a></h3>
            <p class="price_style"><span class="price_style001">10,000円</span><span class="price_style002">(税込)</span></p>
        </div>

        <div class="ranking_list">
            <p class="rank_img">
                <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/ranking_img/ranking_img001.png" alt=""></a>
            </p>
            <h3><a href="#">商品名テキスト商品名テキスト商品名テキスト商品名テキスト</a></h3>
            <p class="price_style"><span class="price_style001">10,000円</span><span class="price_style002">(税込)</span></p>
        </div>

    </div>
</div>

<!-- ################ ranking ################ -->


<!-- ################ project ################ -->

<div class="index_project_sp">
    <div class="title_sp01 project_title">
        <div class="title_sp01_02">
            <h2>PROJECT</h2>
            <p>「らしさがあふれる、世界を。」をビジョンに、DIYを文化にしていく活動をご紹介します。</p>
        </div>
    </div>

    <div class="index_project_sp02 pc">
        <p class="index_project_img">
            <a href="#" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/project_img/project_img001.png" alt=""></a>
        </p>
        <div class="index_project_sp03">
            <h3>DIY × KIDS</h3>
            <p>「未来をつくる子どもたちにDIYの楽しさを」<br> 初めて自分の力でつくる。最初は失敗もする。でも、だんだんコツをつかんでくる。自分でつくったからこそ得られる達成感。湧く愛着。ものを大事にすること。これを子どもに感じてもらって、DIYの楽しさに気付いてほしい。
            </p>
            <p>DIYの楽しさを知ることで住まいに手を加える。修理をする。暮らしを自由に、自分らしくつくりかえる。そんな暮らしぶりが当たり前な社会になるよう、DIYを文化にしたいと考えています。</p>
        </div>
    </div>

    <div class="index_project_sp02 sp">
        <div class="index_project_img">
            <a href="#" class="imghover">
                <h3>DIY × KIDS</h3><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/project_img/project_img001.png" alt=""></a>
        </div>
        <div class="index_project_sp03">
            <p>「未来をつくる子どもたちにDIYの楽しさを」<br> 初めて自分の力でつくる。最初は失敗もする。でも、だんだんコツをつかんでくる。自分でつくったからこそ得られる達成感。湧く愛着。ものを大事にすること。これを子どもに感じてもらって、DIYの楽しさに気付いてほしい。
            </p>
            <p>DIYの楽しさを知ることで住まいに手を加える。修理をする。暮らしを自由に、自分らしくつくりかえる。そんな暮らしぶりが当たり前な社会になるよう、DIYを文化にしたいと考えています。</p>
        </div>
    </div>
</div>

<!-- ################ project ################ -->


<!-- ################ shop ################ -->

<div class="index_shop_sp">
    <div class="title_sp01 shop_title">
        <div class="title_sp01_02">
            <h2>SHOP</h2>
            <p class="pc">二子玉ライズ・ショッピングセンターにある直営店は「DIYで暮らしを豊かに」をコンセプトとした温もりあふれる空間です。<br>様々なイベントと共にみなさまのご来店をお待ちしております。</p>
            <p class="sp center">DIY FACTORY FUTAKOTAMAGAWA</p>
        </div>
    </div>


    <!-- ###### pc_slide ##### -->

    <div class="pc_index_shop_sp02">
        <div class="index_shop_catch">
            <h3>DIY FACTORY FUTAKOTAMAGAWA</h3>
            <p class="btn_style02"><a href="https://www.diyfactory.jp/shoplist/futako/" target="_blank">お店を見る</a></p>
        </div>

        <div id="slider1">
            <div>
                <p><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/shop_img/dff_shop_slide_img001.png" alt="DFFスライドイメージ画像その1"></p>
            </div>
            <div>
                <p><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/shop_img/dff_shop_slide_img002.png" alt="DFFスライドイメージ画像その2"></p>
            </div>
            <div>
                <p><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/shop_img/dff_shop_slide_img003.png" alt="DFFスライドイメージ画像その3"></p>
            </div>
            <div>
                <p><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/shop_img/dff_shop_slide_img004.png" alt="DFFスライドイメージ画像その4"></p>
            </div>
            <div>
                <p><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/shop_img/dff_shop_slide_img005.png" alt="DFFスライドイメージ画像その5"></p>
            </div>
        </div>

    </div>

    <!-- ###### pc_slide ##### -->

    <!-- ###### sp_slide ##### -->

    <div class="sp_index_shop_sp02">

        <div id="slider2">
            <div>
                <p><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/shop_img/sp/sp_dff_shop_slide_img001.png" alt="スマホDFFスライドイメージ画像その1"></p>
            </div>
            <div>
                <p><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/shop_img/sp/sp_dff_shop_slide_img002.png" alt="スマホDFFスライドイメージ画像その2"></p>
            </div>
            <div>
                <p><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/shop_img/sp/sp_dff_shop_slide_img003.png" alt="スマホDFFスライドイメージ画像その3"></p>
            </div>
            <div>
                <p><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/shop_img/sp/sp_dff_shop_slide_img004.png" alt="スマホDFFスライドイメージ画像その4"></p>
            </div>
            <div>
                <p><img src="<?php echo get_template_directory_uri(); ?>/img/index_img/shop_img/sp/sp_dff_shop_slide_img005.png" alt="スマホDFFスライドイメージ画像その5"></p>
            </div>
        </div>

    </div>

    <!-- ###### sp_slide ##### -->

    <div class="index_shop_sp03">
        <p>二子玉ライズ・ショッピングセンターにある直営店は「DIYで暮らしを豊かに」をコンセプトとした温もりあふれる空間です。様々なイベントと共にみなさまのご来店をお待ちしております。</p>
        <p class="btn_style01"><a href="https://www.diyfactory.jp/shoplist/futako/" target="_blank">お店を見る</a></p>
    </div>
</div>

<!-- ################ shop ################ -->


<!-- ################ search ################ -->

<div class="index_search_sp">
    <div class="title_sp01 search_title">
        <div class="title_sp01_02">
            <h2>SEARCH</h2>
            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
        </div>
    </div>

    <div class="index_search_sp02">

        <div class="search_list">
            <h3>パーツから探す</h3>
            <ul>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/parts/search_pa_img001.png" alt="木材">木材</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/parts/search_pa_img001.png" alt="塗料">塗料</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/parts/search_pa_img001.png" alt="金具">金具</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/parts/search_pa_img001.png" alt="工具">工具</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/parts/search_pa_img001.png" alt="装飾">装飾</a>
                </li>
            </ul>
        </div>

        <div class="search_list">
            <h3>コーデから探す</h3>
            <ul>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/coordinate/search_co_img001.png" alt="収納">収納</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/coordinate/search_co_img001.png" alt="家具">家具</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/coordinate/search_co_img001.png" alt="インテリア">インテリア</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/coordinate/search_co_img001.png" alt="エクステリア">エクステリア</a>
                </li>
            </ul>
        </div>

        <div class="search_list">
            <h3>シーンから探す</h3>
            <ul>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="玄関">玄関</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="トイレ">トイレ</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="キッチン">キッチン</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="リビング">リビング・ダイニング</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="ランドリー">ランドリー</a>
                </li>
            </ul>
        </div>

        <div class="search_list">
            <h3 class="pc"></h3>
            <ul>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="子ども部屋">子ども部屋</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="寝室">寝室</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="ベランダ">ベランダ</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/scene/search_sc_img001.png" alt="庭">庭</a>
                </li>
            </ul>
        </div>

        <div class="search_list">
            <h3>プロジェクトから探す</h3>
            <ul>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/project/search_pr_img001.png" alt="KIDS">DIY×KIDS</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/project/search_pr_img001.png" alt="賃貸">DIY×賃貸</a>
                </li>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/project/search_pr_img001.png" alt="GREEN">DIY×GREEN</a>
                </li>
            </ul>
        </div>

        <div class="search_list">
            <h3>DIYerから探す</h3>
            <ul>
                <li>
                    <a href="https://shop.diyfactory.jp/category" class="imghover"><img src="<?php echo get_template_directory_uri(); ?>/common/img/search_img/diyer/search_di_img001.png" alt="コーラル">コーラル</a>
                </li>
            </ul>
        </div>

    </div>
</div>

<!-- ################ search ################ -->


</div>

<!-- ################ index_contents ################ -->

<?php get_footer(); ?>